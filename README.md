# noise-audio-js

Wrapper for `<audio>` element.

## References:

* [https://medium.com/the-andela-way/build-and-publish-your-first-npm-package-a4daf0e2431](https://medium.com/the-andela-way/build-and-publish-your-first-npm-package-a4daf0e2431)
* [https://dev.to/therealdanvega/creating-your-first-npm-package-2ehf](https://dev.to/therealdanvega/creating-your-first-npm-package-2ehf)
* [https://www.freecodecamp.org/news/how-to-make-a-beautiful-tiny-npm-package-and-publish-it-2881d4307f78/](https://www.freecodecamp.org/news/how-to-make-a-beautiful-tiny-npm-package-and-publish-it-2881d4307f78/)
* [https://docs.npmjs.com/creating-node-js-modules](https://docs.npmjs.com/creating-node-js-modules)
