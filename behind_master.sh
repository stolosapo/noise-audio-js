#!/usr/bin/env bash

git fetch
COMMITS_BEHIND="$(git rev-list --left-right --count origin/master...@ | cut -f1)"

if (( COMMITS_BEHIND == 0 )); then
    echo "Branch is up to date with master."
    exit 0
fi

echo "Branch is "${COMMITS_BEHIND}" commit(s) behind master. Please merge from master."
exit 1
